from kukulkan.api.model.attribute import Attribute


class Integer(Attribute):
    """An integer attribute."""
