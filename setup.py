from setuptools import setup, find_packages


setup(
    name='kukulkan-tests-attribute-types',
    version='0.1.0',
    description='',
    package_dir={'': 'src'},
    include_package_data=True,
    packages=find_packages('src'),
    entry_points={'kukulkan.attributes': 'Integer = attribute_types.attribute_types:Integer'},
    license='MIT',
    url='https://gitlab.com/Aphosis/kukulkan-tests-attribute-types',
    download_url='git+https://gitlab.com/Aphosis/kukulkan-tests-attribute-types.git#egg=kukulkan-tests-attribute-types',
    install_requires=['kukulkan-tests'],
    dependency_links=['https://gitlab.com/Aphosis/kukulkan-tests/repository/master/archive.zip#egg=kukulkan-tests']
)
